import asyncio

from textual import work
from textual.binding import Binding
from textual.containers import Horizontal
from textual.widgets import TabPane

from tooi.api import APIError
from tooi.api.timeline import Timeline
from tooi.context import get_context, is_mine
from tooi.data import events, statuses
from tooi.data.events import Event
from tooi.data.instance import InstanceInfo
from tooi.entities import Status
from tooi.messages import EventHighlighted, EventSelected, ShowError, ShowImages, StatusReply, ShowStatusMessage
from tooi.messages import ShowAccount, ShowSource, ShowStatusMenu, ShowThread, ToggleStatusFavourite
from tooi.messages import ToggleStatusBoost, EventMessage, StatusEdit
from tooi.widgets.dialog import DeleteStatusDialog
from tooi.widgets.event_detail import make_event_detail, EventDetailPlaceholder
from tooi.widgets.event_list import EventList
from tooi.widgets.status_detail import StatusDetail


class TimelineTab(TabPane):
    """
    A tab that shows events from a timeline.
    """

    BINDINGS = [
        Binding("a", "show_account", "Account"),
        Binding("u", "show_source", "Source"),
        Binding("t", "show_thread", "Thread"),
        Binding("m", "show_media", "View media"),
        Binding("e", "status_edit", "Edit"),
        Binding("r", "status_reply", "Reply"),
        Binding("f", "status_favourite", "(Un)Favourite"),
        Binding("b", "status_boost", "(Un)Boost"),
        Binding("d", "status_delete", "Delete"),

        Binding("left,h", "scroll_left", "Scroll Left", show=False),
        Binding("right,l", "scroll_right", "Scroll Right", show=False),
        Binding("s", "show_sensitive", "Show Sensitive", show=False),
    ]

    class NewEventPosted(EventMessage):
        pass

    class EventUpdated(EventMessage):
        pass

    class EventDeleted(EventMessage):
        pass

    def __init__(
        self,
        instance: InstanceInfo,
        timeline: Timeline,
        *,
        initial_focus: str | None = None,
        title: str | None = None,
        id: str | None = None,
    ):
        super().__init__(title or timeline.name)

        self.context = get_context()
        self.timeline = timeline
        self.generator = timeline.fetch()
        self.fetching = False
        self.initial_focus = initial_focus
        self.instance = instance

        if self.context.config.options.always_show_sensitive is not None:
            self.always_show_sensitive = self.context.config.options.always_show_sensitive
        else:
            self.always_show_sensitive = instance.get_always_show_sensitive()

        # Start with an empty status list while we wait to load statuses.
        self.event_list = EventList([])
        self.event_detail = EventDetailPlaceholder()

    def batch_show_update(self):
        self.event_list.focus()

    async def on_mount(self):
        self.event_detail.focus()
        await self.fetch_timeline()
        if self.initial_focus:
            self.event_list.focus_event(self.initial_focus)

        # Start our background worker to load new statuses.  We start this even if the timeline
        # can't update, because it may have some other way to acquire new events.
        self.fetch_events()

        # Start the timeline periodic refresh, if configured.
        if self.timeline.can_update and self.context.config.options.timeline_refresh > 0:
            self.timeline.periodic_refresh(self.context.config.options.timeline_refresh)

        # Start streaming.
        if self.context.config.options.streaming and self.timeline.can_stream:
            await self.timeline.streaming(True)

    async def on_unmount(self):
        await self.timeline.close()

    def compose(self):
        yield Horizontal(
            self.event_list,
            self.event_detail,
            id="main_window"
        )

    def on_new_event_posted(self, message: NewEventPosted):
        self.event_list.prepend_events([message.event])
        self.query_one(EventList).refresh_events()

    @work(group="fetch_events")
    async def fetch_events(self):
        # Fetch new events from the timeline and post messages for them.  This task runs in a
        # separate async task, so we don't want to touch the UI directly.
        while events := await self.timeline.get_events_wait():
            for event in events:
                self.post_message(TimelineTab.NewEventPosted(event))

    async def refresh_timeline(self):
        # Handle timelines that don't support updating.
        if not self.timeline.can_update:
            await self.fetch_timeline()
        else:
            # This returns immediately; any updates will be handled by fetch_events.
            await self.timeline.update()

    async def fetch_timeline(self):
        try:
            events = await anext(self.generator)
        except StopAsyncIteration:
            events = []  # No statuses
        except APIError as exc:
            self.post_message(ShowStatusMessage(f"[red]Could not load timeline: {str(exc)}[/]"))
            return

        self.event_list.replace(events)
        self.query_one("#main_window").mount(self.event_detail)

    def on_event_highlighted(self, message: EventHighlighted):
        # Update event details only if focused event has changed
        current_event = self.event_detail.event
        if not current_event or current_event.id != message.event.id:
            self.show_event_detail(message.event)

    @work(exclusive=True, group="show_event_detail")
    async def show_event_detail(self, event: Event):
        # TODO: This is slow, try updating the existing EventDetail instead of
        # creating a new one. This requires some fiddling since compose() is
        # called only once, so updating needs to be implemented manually.
        # See: https://github.com/Textualize/textual/discussions/1683

        # Having a short sleep here allows for smooth scrolling. Since `@work`
        # has `exclusive=True` this task will be canceled if it is called again
        # before the current one finishes. When scrolling down the event list
        # quickly, this happens before the sleep ends so the status is not
        # drawn at all until we stop scrolling.
        await asyncio.sleep(0.05)

        self.event_detail.remove()
        self.event_detail = make_event_detail(event)
        self.query_one("#main_window").mount(self.event_detail)
        asyncio.create_task(self.maybe_fetch_next_batch())

    def update_event(self, event: Event):
        self.event_list.update_event(event)
        if self.event_detail.event and self.event_detail.event.id == event.id:
            self.event_detail.update_event(event)

    def remove_event(self, event: Event):
        self.event_list.remove_event(event)

    def on_event_selected(self, message: EventSelected):
        if message.event.status:
            self.post_message(ShowStatusMenu(message.event.status))

    async def on_toggle_status_favourite(self, message: ToggleStatusFavourite):
        assert message.event.status
        original = message.event.status.original

        try:
            if original.favourited:
                await statuses.unfavourite(original.id)
                self.post_message(ShowStatusMessage("[green]✓ Status unfavourited[/]", 3))
            else:
                await statuses.favourite(original.id)
                self.post_message(ShowStatusMessage("[green]✓ Status favourited[/]", 3))
        except APIError as exc:
            self.post_message(ShowError("Error", f"Could not (un)favourite status: {str(exc)}"))

        await self._post_event_update(message.event)

    async def action_status_favourite(self):
        if (event := self.event_list.current) and event.status is not None:
            self.post_message(ToggleStatusFavourite(event))

    async def on_toggle_status_boost(self, message: ToggleStatusBoost):
        assert message.event.status
        original = message.event.status.original

        try:
            if original.reblogged:
                await statuses.unboost(original.id)
                self.post_message(ShowStatusMessage("[green]✓ Status unboosted[/]", 3))
            else:
                await statuses.boost(original.id)
                self.post_message(ShowStatusMessage("[green]✓ Status boosted[/]", 3))
        except APIError as exc:
            self.post_message(ShowError("Error", f"Could not (un)boost status: {str(exc)}"))

        await self._post_event_update(message.event)

    async def _post_event_update(self, event: Event):
        # NB: It might be possible to avoid having to reload the event, but it's
        # way more complicated than it seems at first. For an attempt, which
        # doesn't event work with notification events, see here:
        # https://paste.sr.ht/~ihabunek/f160e10528f71ed3eef67dbe8c74cb569dc62c9f
        updated_event = await events.reload(event)
        self.post_message(TimelineTab.EventUpdated(updated_event))

    def action_status_boost(self):
        if (event := self.event_list.current) and event.status is not None:
            self.post_message(ToggleStatusBoost(event))

    def action_status_delete(self):
        if (event := self.event_list.current) and (status := event.status) and is_mine(status):
            def on_dismiss(deleted: bool):
                if deleted:
                    self.post_message(TimelineTab.EventDeleted(event))
            self.app.push_screen(DeleteStatusDialog(status), on_dismiss)

    def action_show_sensitive(self):
        if isinstance(self.event_detail, StatusDetail) and self.event_detail.sensitive:
            self.event_detail.reveal()

    def action_show_account(self):
        if status := self._current_status():
            self.post_message(ShowAccount(status.original.account))

    def action_show_source(self):
        if status := self._current_status():
            self.post_message(ShowSource(status))

    def action_show_thread(self):
        if status := self._current_status():
            self.post_message(ShowThread(status))

    async def action_status_edit(self):
        if status := self._current_status():
            source = await statuses.source(status.original.id)
            self.post_message(StatusEdit(status.original, source))

    def action_status_reply(self):
        if status := self._current_status():
            self.post_message(StatusReply(status))

    def action_scroll_left(self):
        self.event_list.focus()

    def action_scroll_right(self):
        self.event_detail.focus()

    def action_show_media(self):
        if status := self._current_status():
            urls = [e.url for e in status.original.media_attachments if e.type == "image"]
            if urls:
                self.post_message(ShowImages(urls))

    async def maybe_fetch_next_batch(self):
        if self.should_fetch():
            self.fetching = True
            self.post_message(ShowStatusMessage("[green]Loading statuses...[/]"))
            # TODO: handle exceptions
            try:
                next_events = await anext(self.generator)
                self.event_list.append_events(next_events)
            finally:
                self.post_message(ShowStatusMessage())
                self.fetching = False

    def should_fetch(self):
        if not self.fetching and self.event_list.index is not None:
            diff = self.event_list.count - self.event_list.index
            return diff < 10

    def _current_status(self) -> Status | None:
        if event := self.event_list.current:
            return event.status
